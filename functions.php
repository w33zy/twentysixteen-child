<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

if ( !function_exists( 'chld_thm_cfg_child_css' ) ):
    function chld_thm_cfg_child_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', get_stylesheet_uri() ); 
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_child_css', 999 );
// END ENQUEUE PARENT ACTION


// BEGIN CUSTOMIZER UPLOAD LOGO FUNCTION
function twentysixteen_child_customize_register($wp_customize){

	$wp_customize->add_section('tsc_logo_upload', array(
		'title'    => __('Logo Upload', 'twentysixteen-child'),
		'description' => '',
		'priority' => 120, //Set to low number to move option upwards in the menu
	));

	$wp_customize->add_setting('twentysixteen-child-logo', array(
		'default'           => '',
		'capability'        => 'edit_theme_options',
		'type'           => 'theme_mod',
	));

	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo-upload', array(
		'label'    => __('Logo Upload', 'twentysixteen-child'),
		'section'  => 'tsc_logo_upload',
		'settings' => 'twentysixteen-child-logo',
		'context'  => 'twentysixteen-child-custom-logo',
	)));

}
add_action('customize_register', 'twentysixteen_child_customize_register');
// END CUSTOMIZER UPLOAD LOGO FUNCTION